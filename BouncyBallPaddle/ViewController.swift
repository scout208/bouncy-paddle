//
//  ViewController.swift
//  BouncyBallPaddle
//
//  Created by uics14 on 11/29/15.
//  Copyright © 2015 uics14. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollisionBehaviorDelegate {
    
    var animator: UIDynamicAnimator!
    var gravity: UIGravityBehavior!
    var collision: UICollisionBehavior!
    var push: UIPushBehavior!
    var attachment: UIAttachmentBehavior!
    
    var attached: Bool!
    var player1Ball: Bool!
    var player2Ball: Bool!
    
    var rotate: CGAffineTransform!
    var translate: CGAffineTransform!
    
    var ballBehavior: UIDynamicItemBehavior!
    
    var player1ScoreVal = 0
    var player2ScoreVal = 0
    
    @IBOutlet weak var ball: UIView!
    
    @IBOutlet weak var player1Paddle: UIView!
    @IBOutlet weak var player2Paddle: UIView!
    
    @IBOutlet weak var player1Screen: UIView!
    @IBOutlet weak var player2Screen: UIView!
    
    @IBOutlet weak var player1ScoreLabel: UILabel!
    @IBOutlet weak var player2ScoreLabel: UILabel!
    
    @IBOutlet weak var player1Score: UILabel!
    @IBOutlet weak var player2Score: UILabel!
    
    
    @IBOutlet weak var playerWins: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        rotate = CGAffineTransformMakeRotation(CGFloat(-M_PI_2))
        
        translate = CGAffineTransformMakeTranslation((player1ScoreLabel.bounds.height / 2)-(player1ScoreLabel.bounds.width / 2), (player1ScoreLabel.bounds.width / 2)-(player1ScoreLabel.bounds.height / 2))
        player1ScoreLabel.transform = CGAffineTransformConcat(rotate, translate)
        
        translate = CGAffineTransformMakeTranslation((player2ScoreLabel.bounds.height / 2)-(player2ScoreLabel.bounds.width / 2), (player2ScoreLabel.bounds.width / 2)-(player2ScoreLabel.bounds.height / 2))
        player2ScoreLabel.transform = CGAffineTransformConcat(rotate, translate)
        
        
        translate = CGAffineTransformMakeTranslation((player1Score.bounds.height / 2)-(player1Score.bounds.width / 2), (player1Score.bounds.width / 2)-(player1Score.bounds.height / 2))
        player1Score.transform = CGAffineTransformConcat(rotate, translate)
        
        translate = CGAffineTransformMakeTranslation((player2Score.bounds.height / 2)-(player2Score.bounds.width / 2), (player2Score.bounds.width / 2)-(player2Score.bounds.height / 2))
        player2Score.transform = CGAffineTransformConcat(rotate, translate)
        
        translate = CGAffineTransformMakeTranslation((playerWins.bounds.height / 2)-(playerWins.bounds.width / 2), (playerWins.bounds.width / 2)-(playerWins.bounds.height / 2))
        playerWins.transform = CGAffineTransformConcat(rotate, translate)
        playerWins.text = ""
        
        // Create an animator to keep track of various behaviors added to UIKit Dynamics engine
        // and provide overall context
        animator = UIDynamicAnimator(referenceView: self.view)
        
        
        // This attachment only gets performed on load
        attachment = UIAttachmentBehavior(item: ball, attachedToItem: player1Paddle)
        animator.addBehavior(attachment)
        attached = true
        player1Ball = true
        
        startAnimating()
    }
    
    
    @IBAction func movePaddle(recognizer: UIPanGestureRecognizer) {
        
        let translation: CGPoint
        let movedFrame: CGRect
        
        if let view = recognizer.view {
            if view == player1Screen {
                translation = recognizer.translationInView(player1Screen)
                movedFrame = CGRectOffset(player1Paddle.frame, translation.x, translation.y)
                
                if CGRectContainsRect(player1Screen.bounds, movedFrame) {
                    player1Paddle.center = CGPoint(x: player1Paddle.center.x + translation.x, y: player1Paddle.center.y + translation.y)
                    
                    animator.updateItemUsingCurrentState(player1Paddle)
                }
            } else if view == player2Screen {
                translation = recognizer.translationInView(player2Screen)
                movedFrame = CGRectOffset(player2Paddle.frame, translation.x, translation.y)
                
                if CGRectContainsRect(player2Screen.bounds, movedFrame) {
                    player2Paddle.center = CGPoint(x: player2Paddle.center.x + translation.x, y: player2Paddle.center.y + translation.y)
                    animator.updateItemUsingCurrentState(player2Paddle)
                }
            }
            
            recognizer.setTranslation(CGPointZero, inView: view)
        }
        
    }
    
    @IBAction func launchBall(recognizer: UITapGestureRecognizer) {
        if attached == true {
            animator.removeBehavior(attachment)
            self.view.addSubview(ball)
            if let view = recognizer.view {
                if view == player1Screen {
                    if player1Ball == true {
                        animator.addBehavior(push)
                    }
                } else if view == player2Screen {
                    if player2Ball == true {
                        animator.addBehavior(push)
                    }
                }
            }
            startAnimating()
        }
    }
    
    
    
    func collisionBehavior(behavior: UICollisionBehavior, beganContactForItem item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, atPoint p: CGPoint) {
        //print("Boundary contact occurred - \(identifier)")
        
        if let contact = identifier as? String {
            stopAnimating()
            print("Score!")
            if contact == "player1Goal" {
                playerWins.text = "Player 2 Wins!"
            } else if contact == "player2Goal" {
                playerWins.text = "Player 1 Wins!"
            }
            attached = true
        }
    }
    
    func startAnimating() {
        // Start the ball off with a push
        push = UIPushBehavior(items: [ball], mode: UIPushBehaviorMode.Instantaneous)
        push.pushDirection = CGVectorMake(0.25, 0.5)
        
        // Create the collision behavior
        collision = UICollisionBehavior(items: [ball, player1Paddle, player2Paddle])
        collision.collisionDelegate = self
        
        // Edges are bounds too
        collision.translatesReferenceBoundsIntoBoundary = true
        collision.addBoundaryWithIdentifier("player1Goal", fromPoint: CGPointMake(view.bounds.minX, view.bounds.maxY), toPoint: CGPointMake(view.bounds.maxX, view.bounds.maxY))
        collision.addBoundaryWithIdentifier("player2Goal", fromPoint: CGPointMake(view.bounds.minX, view.bounds.minY), toPoint: CGPointMake(view.bounds.maxX, view.bounds.minY))
        animator.addBehavior(collision)
        
        // Update paddle behavior
        let paddleBehavior = UIDynamicItemBehavior(items: [player1Paddle, player2Paddle])
        paddleBehavior.allowsRotation = false
        paddleBehavior.density = 1000
        animator.addBehavior(paddleBehavior)
        
        // Update item behavior
        ballBehavior = UIDynamicItemBehavior(items: [ball])
        ballBehavior.allowsRotation = false
        ballBehavior.elasticity = 1.0
        ballBehavior.friction = 0
        ballBehavior.resistance = 0
        animator.addBehavior(ballBehavior)

    }
    
    func stopAnimating() {
        animator.removeAllBehaviors()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

