# README #

This is a simple interactive 2D animation-based game using UIKit Dynamics. It's basically a pong game that can be played by two players using either side of the device.

The game is not complete, but to play you tap the screen to launch the ball. The ball will launch around and you can move either paddle with your finger. The ball will bounce off the side walls or the paddle. When the ball touches the top or bottom of the screen, the point goes to the other player. 

The scoring display may be gimmicky as well. And I had trouble getting the ball to reset after someone scores a point so it may just be necessary to start a new game.